from iroha import Iroha, IrohaCrypto, IrohaGrpc, primitive_pb2

iroha = Iroha('admin@test')
net = IrohaGrpc('127.0.0.1:50052')

admin_private_key = b'f101537e319568c765b2cc89698325604991dca57b9716b58016b253506cab70'
admin_tx = iroha.transaction(
    [iroha.command('AddAssetQuantity', asset_id='coin#test', amount='100000')]
)
IrohaCrypto.sign_transaction(admin_tx, admin_private_key)
net.send_tx(admin_tx)

for status in net.tx_status_stream(admin_tx):
    print(status)