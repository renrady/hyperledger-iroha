from iroha import Iroha, IrohaCrypto, IrohaGrpc, primitive_pb2

iroha = Iroha('admin@test')
net = IrohaGrpc('149.28.195.166:50051')

peer = primitive_pb2.Peer()
peer.address = '149.28.195.166:10005'
peer.peer_key = b'380fbb2b022b8099cfa4183fcecb4a7be10a16b3aa1f68de543df2c476124294'

alice_key = b'f101537e319568c765b2cc89698325604991dca57b9716b58016b253506cab70'
alice_tx = iroha.transaction(
    [iroha.command(
        'AddPeer', 
        peer=peer
    )]
)
IrohaCrypto.sign_transaction(alice_tx, alice_key)
net.send_tx(alice_tx)

for status in net.tx_status_stream(alice_tx):
    print(status)