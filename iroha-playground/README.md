# Iroha-playground

This project is intended to help those who wishes to have a quick guide on starting with Hyperledger Iroha.

At the time of this project initialization, the official documentation hasn't fully explain nor provide working example regarding manual deployment of multiple peer.

You can view the official document [here](https://iroha.readthedocs.io/en/latest/index.html).

_Everything detailed here is meant to be used with the provided projects._

## 1. Single Node Deployment

You can find the official explanation and tutorial of a single node [here](https://iroha.readthedocs.io/en/latest/getting_started/index.html).

**1.1 Create a Docker Network**

Iroha requires a _PostgreSQL_ to operate.

```
    docker network create iroha-network
```

**1.2 Start PostgreSQL Container**

If you already have a container using port `5432`, you can switch it to something like `5433:5432`.

```
    docker run --name some-postgres \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -p 5432:5432 \
    --network=iroha-network \
    -d postgres:9.5 \
    -c 'max_prepared_transactions=100'
```

**1.3 Create Blockstore**

Iroha will store its successful transaction in blocks for the chain. So we need to create a volume for the node.

```
    docker volume create blockstore
```

**1.4 Start Iroha Container**

Once everything is ready, we can start the Iroha container using the configuration from the folder `node0`

```
    docker run -it --name iroha \
    -p 50051:50051 \
    -v ~/iroha-playground/node0:/opt/iroha_data \
    -v blockstore:/tmp/blockstore \
    --network=iroha-network \
    --entrypoint=/bin/bash \
    hyperledger/iroha:latest
```

Make sure the Path to the project is correct

> -v ~/iroha-playground/node0:/opt/iroha_data \

Normally, there is an `entrypoint.sh` file to automatically start the node. But we use the below code to start the node **manually**.

> --entrypoint=/bin/bash \

**1.5 Start Iroha Node**

Now that we have attached Iroha to a container. We can start the node by running the code below.

`irohad --config config.docker --genesis_block genesis.block --keypair_name node0`

**1.6 Performing Transaction and Query**

This project will not go into how to make transaction and Query as the purpose for this project to get up-and-running fast.

You can use the official documentation [here](https://iroha.readthedocs.io/en/latest/getting_started/cli-guide.html) to start making transactions.

## 2. Multiple Peers (nodes) manual deployment

The code that is used for this guide is provided by Nikolay which was used during Hyperledger Iroha monthly meet up in Cambodia.

In this guide, we will be deploying up to 4 instances of Iroha node.

_Importance: Make sure there are no containers with the same names and ports as these nodes._

**2.1 Create a Docker Network**

`docker network create iroha-network`

**2.2 Start PostgreSQL Container**

```
    docker run --name some-postgres \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -p 5432:5432 \
    --network=iroha-network \
    -d postgres:9.5 \
    -c 'max_prepared_transactions=100'
```

**2.3 Create Blockstore**

Each node will require an instace of its own **blockstore**. Therefore, we will be creating four instances for each of our node.

Node1

`docker volume create blockstore_1`

Node2

`docker volume create blockstore_2`

Node3

`docker volume create blockstore_3`

Node4

`docker volume create blockstore_4`

**2.4 Start Iroha Containers**

We will be starting these node a bit different from the example of Single Node.

We won't be using this line of code as to avoid having to start all four nodes manually.

> --entrypoint=/bin/bash \

We will let the script `entrypoint.sh` run the Iroha node for us.

You will also notice this line of code.

> -d hyperledger/iroha:1.0.0_rc5

We will be using this version of Iroha as it is a stable release at the time of this writing.

Node1

```
    docker run --name iroha-1 \
    -p 50051:50051 \
    -p 10001:10001 \
    -v ~/iroha-playground/meetup/one_peer:/opt/iroha_data \
    -v blockstore_1:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='node0' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0_rc5
```

Node2

```
    docker run --name iroha-2 \
    -p 50052:50052 \
    -p 10002:10002 \
    -v ~/iroha-playground/meetup/two_peer:/opt/iroha_data \
    -v blockstore_2:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='node1' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0_rc5
```

Node3

```
    docker run --name iroha-3 \
    -p 50053:50053 \
    -p 10003:10003 \
    -v ~/iroha-playground/meetup/three_peer:/opt/iroha_data \
    -v blockstore_3:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='node2' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0_rc5
```

Node4

```
    docker run --name iroha-4 \
    -p 50054:50054 \
    -p 10004:10004 \
    -v ~/iroha-playground/meetup/four_peer:/opt/iroha_data \
    -v blockstore_4:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='node3' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0_rc5
```

**2.5 Testing with multiple peer**

If you look at the folder of each nodes, you will notice that _one_peer_ folder contains these two files:

> admin@test.priv

> admin@test.pub

You will need these keys to log into iroha as admin user.
