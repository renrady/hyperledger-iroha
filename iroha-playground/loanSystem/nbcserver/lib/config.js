var grpc = require("grpc");
var iroha = require("iroha-helpers");
var irohaServices = require("iroha-helpers/lib/proto/endpoint_grpc_pb");

var txHelper = require("iroha-helpers/lib/txHelper.js");

const QueryService_v1Client = irohaServices.QueryService_v1Client;
const CommandService_v1Client = irohaServices.CommandService_v1Client;

const queries = iroha.queries;
const commands = iroha.commands;

const IROHA_ADDRESS = "159.65.135.72:50054";

const queryService = new QueryService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

const commandService = new CommandService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

const createCommandOption = (key, id, quorum = 1) => {
  return {
    privateKeys: [key],
    creatorAccountId: id,
    quorum: quorum,
    commandService,
    timeoutLimit: 5000
  };
};

const createQueryOption = (key, id) => {
  return {
    privateKey: key,
    creatorAccountId: id,
    queryService,
    timeoutLimit: 5000
  };
};

const SignAndSend = () => {};

module.exports = {
  queries,
  commands,
  createCommandOption,
  createQueryOption,
  commandService
};
