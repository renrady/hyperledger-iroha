// Iroha Related

const irohaUtil = require("./config");

const {
  queries,
  commands,
  createCommandOption,
  createQueryOption,
  commandService
} = irohaUtil;

const RolePermission = require("iroha-helpers/lib/proto/primitive_pb");

const createResponseMessage = (
  payload,
  message = "Success",
  isSuccess = true
) => {
  return {
    message: message,
    data: payload,
    isSuccess
  };
};

const createSigningResponseObject = (status, message) => {
  return {
    status,
    message
  };
};

const txHelper = require("iroha-helpers/lib/txHelper.js");
const { sign } = txHelper.default;

const { cloneDeep, findIndex, find } = require("lodash");

var txUtil = require("iroha-helpers/lib/util.js");

const keys = {
  "admin@nbc":
    "b8a5efad36e965fbcdd0e359ad9cd7bb9eefaa64b9e99dc687863a63347fcc5a",
  "user@zoom":
    "3b84f5459c44c76b14dd39bc8618ecfa335ba3373c3c8b9486b308335da3da49",
  user: "eb3316be35b72b9957f061f05020435193469f20d77c47aa647075a5a46b7bd2",
  "admin@nbc":
    "b8a5efad36e965fbcdd0e359ad9cd7bb9eefaa64b9e99dc687863a63347fcc5a"
};

async function getAccountBalance(req, res) {
  try {
    console.log("===> Getting Account Balance: ", req.headers.username);
    // return;
    const response = await queries.getAccountAssets(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {
        accountId: req.headers.username
      }
    );

    console.log("===> Response Getting Account Balance: ", response);

    const payloadToResponse = createResponseMessage(response[0], "Wallet Info");
    res.send(payloadToResponse);
  } catch (err) {
    console.log("===> Get Account Detail Error: ", err);
  }
}

async function getSignatories(req, res) {
  try {
    const response = await queries.getSignatories(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {
        accountId: req.headers.username
      }
    );
    res.status(200).send(createResponseMessage(response, "Success"));
  } catch (err) {
    console.log("GetSignatoriesError: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

async function getPendingTransactions(req, res) {
  try {
    console.log("Getting transaction for : ", req.headers.username);
    const response = await queries.getPendingTransactions(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {}
    );

    res.status(200).send(createResponseMessage(response, "Success"));
  } catch (err) {
    console.log("GetPendingTransactionsError: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

async function signTransaction(payload) {
  try {
    console.log("===> Retrieve All Pending Transactions");
    // console.log("With Payload: ", payload);

    const readablePendingTx = await queries.getPendingTransactions(
      createQueryOption(keys["admin@nbc"], payload.user),
      {}
    );

    console.log("PendingTxFound: ", readablePendingTx.length);
    // console.log("PendingTxFound: ", readablePendingTx);

    // findIndex(readablePendingTx, tx => {
    //   console.log("tx: ", tx.signaturesList);
    //   return tx.signaturesList === [];
    // });

    const matchTxIndex = findIndex(readablePendingTx, tx =>
      find(
        tx.signaturesList,
        signature => signature.publicKey === payload.publicKey
      )
    );

    if (matchTxIndex === -1)
      return createSigningResponseObject(201, "No Pending Transaction Found");

    console.log("Matched Transaction Found. For user ", payload.user);
    const response = await queries.getRawPendingTransactions(
      createQueryOption(keys["admin@nbc"], payload.user),
      {}
    );

    // if (response.length < 1)
    //   return {
    //     ...createResponseMessage(null, "No Transaction Found"),
    //     data: {}
    //   };
    // res
    //   .status(201)
    //   .send(createResponseMessage(null, "No Pending Transactions"));

    const txPending = response.getTransactionsList()[matchTxIndex];

    console.log("Matched Tx: ", txPending.toObject());

    txPending.clearSignaturesList();

    console.log("===> Signing Transaction ");

    let copyTx = sign(txPending, keys["admin@nbc"]);

    const signingResponse = await txUtil.sendTransactions(
      [copyTx],
      commandService,
      5000
    );

    console.log("Success: ", signingResponse);

    return createSigningResponseObject(200, "Success");
  } catch (err) {
    console.log("SignTransactionError: ", err);
    // res.status(400).send(createResponseMessage(err, "Sign Failed"));
    return createSigningResponseObject(500, "Something went wrong");
  }
}

module.exports = {
  getAccountBalance,
  getPendingTransactions,
  getSignatories,
  signTransaction
};
