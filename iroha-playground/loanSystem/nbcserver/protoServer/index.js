var PROTO_PATHS = [
  "/root/iroha-playground/loanSystem/nbcserver/proto/signatory.proto"
];
var grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");

var irohaFunction = require("../lib");

// Create Server Instance
var server = new grpc.Server();

// Load Package
var packageDefinition = protoLoader.loadSync(PROTO_PATHS);

// Create Package Instance
var SignatoryService = grpc.loadPackageDefinition(packageDefinition).Signatory;

async function onSigningRequest(call, callback) {
  try {
    console.log("===> Transaction requires Signing");

    const payload = call.request;
    const response = await irohaFunction.signTransaction(payload);

    console.log("===> Signing Transaction Completed: ", response);

    callback(null, response);
  } catch (err) {
    console.log("SigningTransactionError: ", err);
    callback({ message: "Signing Error", status: "500", data: err }, null);
  }
}

server.addService(SignatoryService.TransactionSigning.service, {
  sign: onSigningRequest
});

server.bind("159.65.135.72:50055", grpc.ServerCredentials.createInsecure());
// server.start();

module.exports = server;
