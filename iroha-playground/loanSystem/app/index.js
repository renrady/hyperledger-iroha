var express = require("express");
var app = express();
var router = express.Router();
var bodyParser = require("body-parser");

// Iroha Related
var grpc = require("grpc");
var iroha = require("iroha-helpers");
var irohaServices = require("iroha-helpers/lib/proto/endpoint_grpc_pb");

const QueryService_v1Client = irohaServices.QueryService_v1Client;
const CommandService_v1Client = irohaServices.CommandService_v1Client;

const queries = iroha.queries;
const commands = iroha.commands;

const IROHA_ADDRESS = "159.65.135.72:50051";

const queryService = new QueryService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

const commandService = new CommandService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

async function getAccountBalance(req, res) {
  try {
    console.log("===> Getting Account Balance: ", req);
    return;
    const response = await queries.getAccountAssets(
      {
        privateKey: req.header.key,
        creatorAccountId: req.header.username,
        queryService,
        timeoutLimit: 5000
      },
      {
        accountId: req.header.username
      }
    );
  } catch (err) {
    console.log("===> Get Account Detail Error: ", err);
  }
}

async function transferTo(req, res) {
  try {
    const response = await commands.transferAsset(
      {
        privateKeys: [req.header.key],
        creatorAccountId: req.body.username,
        quorum: 1,
        commandService,
        timeoutLimit: 5000
      },
      {
        srcAccountId: req.body.username,
        destAccountId: req.body.to,
        assetId: "usd#zoom",
        description: "testing Javascript",
        amount: req.body.amount
      }
    );
  } catch (err) {
    console.log("===> Transafer Error: ", err);
  }
}

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(function(req, res) {
  console.log("Init App");
  res.setHeader("Content-Type", "text/plain");
  res.write("you posted:\n");
  res.end(JSON.stringify(req.body, null, 2));
});

// route middleware that will happen on every request
router.use(function(req, res, next) {
  // log each request to the console
  console.log(req.method, req.url);

  // continue doing what we were doing and go to the route
  next();
});

// home page route (http://localhost:8080)
router.get("/", function(req, res) {
  res.send("im the home page!");
});

const routeWallet = router.get("/wallet", getAccountBalance);

router.post("/transfer", transferTo);

router.post(function(req, res) {
  console.log("processing");
  console.log("===> Get payloads from frontend", req.body);
  res.send("processing the login form!");
});

// apply the routes to our application
app.use("/", router);
app.use("/wallet", routeWallet);

app.listen(3001);
