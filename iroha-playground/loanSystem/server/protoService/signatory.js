var PROTO_PATHS = [
  "/root/iroha-playground/loanSystem/server/proto/signatory.proto"
];
var grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");

var packageDefinition = protoLoader.loadSync(PROTO_PATHS);
var SignatoryService = grpc.loadPackageDefinition(packageDefinition).Signatory;

var client = new SignatoryService.TransactionSigning(
  "159.65.135.72:50055",
  grpc.credentials.createInsecure()
);

// const createSigningResponseObject = (payload) => {}

async function RequestForSignature(payload) {
  try {
    client.sign(payload, (error, response) => {});
  } catch (err) {
    console.log("RequestForSignatureError: ", err);
  }
}

module.exports = client;
