var grpc = require("grpc");
var iroha = require("iroha-helpers");
var irohaServices = require("iroha-helpers/lib/proto/endpoint_grpc_pb");

var txHelper = require("iroha-helpers/lib/txHelper.js");

const QueryService_v1Client = irohaServices.QueryService_v1Client;
const CommandService_v1Client = irohaServices.CommandService_v1Client;

const queries = iroha.queries;
const commands = iroha.commands;

const IROHA_ADDRESS = "159.65.135.72:50051";

const queryService = new QueryService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

const commandService = new CommandService_v1Client(
  IROHA_ADDRESS,
  grpc.credentials.createInsecure()
);

const createCommandOption = (key, id, quorum = 1) => {
  return {
    privateKeys: [key],
    creatorAccountId: id,
    quorum: quorum,
    commandService,
    timeoutLimit: 5000
  };
};

const createQueryOption = (key, id) => {
  return {
    privateKey: key,
    creatorAccountId: id,
    queryService,
    timeoutLimit: 5000
  };
};

const SignAndSend = () => {};

const keys = {
  "admin@zoom":
    "24af596d6416533490b28abbd6ec8fd6d57092584f0faf72303a10b492602143",
  "admin@nbc":
    "b8a5efad36e965fbcdd0e359ad9cd7bb9eefaa64b9e99dc687863a63347fcc5a"
};

var pubKeys = {
  "admin@zoom":
    "af6019c5d9b65f6a68601555198e661bcc73bfd649f194bfe542d9e62dccf644"
};

module.exports = {
  queries,
  commands,
  createCommandOption,
  createQueryOption,
  commandService,
  keys,
  pubKeys
};
