// Iroha Related

const irohaUtil = require("./config");

const {
  queries,
  commands,
  createCommandOption,
  createQueryOption,
  commandService
} = irohaUtil;

// const RolePermission = require("iroha-helpers/lib/proto/primitive_pb");

const { generateKeyPair } = require("iroha-helpers/lib/cryptoHelper.js");

var txHelper = require("iroha-helpers/lib/txHelper.js");

const { sign, addMeta, emptyTransaction, addCommand } = txHelper.default;

const { cloneDeep } = require("lodash");

var txUtil = require("iroha-helpers/lib/util.js");

const validate = require("iroha-helpers/lib/validate");

const signatoryClientService = require("../protoService/signatory.js");

const createResponseMessage = (payload, message = "Success") => {
  return {
    message: message,
    data: payload
  };
};

var keys = {
  "admin@zoom":
    "24af596d6416533490b28abbd6ec8fd6d57092584f0faf72303a10b492602143",
  "user@zoom":
    "3b84f5459c44c76b14dd39bc8618ecfa335ba3373c3c8b9486b308335da3da49",
  user: "eb3316be35b72b9957f061f05020435193469f20d77c47aa647075a5a46b7bd2",
  "admin@nbc":
    "b8a5efad36e965fbcdd0e359ad9cd7bb9eefaa64b9e99dc687863a63347fcc5a"
};

var pubKeys = {
  "admin@zoom":
    "af6019c5d9b65f6a68601555198e661bcc73bfd649f194bfe542d9e62dccf644",
  "user@zoom":
    "45cc30c273b439fed22d7f5594f158cf405d5b46b02d46062fc0701f6708edf7",
  user: "bed9932eaff4c7afc99d1b2e9a2e7b21c0cb71aee1088fd582bd1134fd19848f"
};

async function createAccount(req, res) {
  try {
    const { username } = req.body;

    const { publicKey, privateKey } = generateKeyPair();

    await commands.createAccount(
      createCommandOption(keys["admin@zoom"], "admin@zoom"),
      {
        accountName: username,
        domainId: "zoom"
      }
    );
  } catch (err) {
    console.log("===> CreateAccountError: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

async function getAccountBalance(req, res) {
  try {
    console.log("===> Getting Account Balance: ", req.headers.username);
    // return;
    const response = await queries.getAccountAssets(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {
        accountId: req.headers.username
      }
    );

    console.log("===> Response Getting Account Balance: ", response);

    const payloadToResponse = createResponseMessage(response[0], "Wallet Info");
    res.status(200).send(payloadToResponse);
  } catch (err) {
    console.log("===> Get Account Detail Error: ", err);
  }
}

async function customTransferTo(req, res) {
  try {
    console.log("ReqQuorum: ", req.headers.quorum);
    if (req.headers.quorum.toString() !== "2") {
      console.log("<<< Traditional Transfer >>>");
      transferTo(req, res);
      return;
    }
    console.log("===> Transferring Asset");
    console.log("Transaction made by ", req.headers.username);
    console.log("Send From: ", req.body.from);
    console.log("Send To: ", req.body.to);
    console.log("Amount: ", req.body.amount);

    // Create Payload
    const payload = {
      srcAccountId: req.body.from,
      destAccountId: `${req.body.to}`,
      assetId: "usd#zoom",
      description: req.body.description,
      amount: req.body.amount.toString()
    };

    // Create Command
    const txCommand = addCommand(
      emptyTransaction(),
      "transferAsset",
      validate.default(payload, [
        "srcAccountId",
        "destAccountId",
        "assetId",
        "description",
        "amount"
      ])
    );

    // Create Empty Transaction with our created Command
    var txToSend = addMeta(txCommand, {
      creatorAccountId: req.headers.username,
      quorum: req.headers.quorum
    });

    // Sign the Transaction with our key
    txToSend = txUtil.signWithArrayOfKeys(txToSend, [keys[req.headers.key]]);

    // Send the Transaction
    txUtil.sendTransactions([txToSend], commandService, 5000, ["COMMITTED"]);

    console.log("===> Transaction Send");

    // Create Payload to request for Signature
    const signatoryPayload = {
      user: req.headers.username,
      publicKey: pubKeys[req.headers.key]
    };

    console.log("===> Requesting Signature from Partner");
    console.log("With Payload: ", signatoryPayload);

    const timeOut = new Date().setSeconds(new Date().getSeconds() + 10);
    // Request Signature from Partner
    signatoryClientService.sign(
      signatoryPayload,
      { deadline: timeOut },
      async (error, response) => {
        console.log("Signature Request Completed");

        if (response) {
          console.log("Is Success, bruh");

          // const new
          // await getAccountBalance(req, res);
          res
            .status(response.status)
            .send(createResponseMessage(null, response.message));
        } else if (error) {
          console.log("Is Error, bruh");
          res.status(500).send(createResponseMessage(error, "Error"));
        }
      }
    );

    console.log("Transfer Asset Complete");
    // res.status(200).send(createResponseMessage(null));
  } catch (err) {
    console.log("CustomTransferToError: ", err);
  }
}

async function transferTo(req, res) {
  try {
    console.log("Transaction made by ", req.headers.username);
    console.log("Send From: ", req.body.from);
    console.log("Send To: ", req.body.to);
    console.log("Amount: ", req.body.amount);
    // res.send("Pfft");
    // return;

    await commands.transferAsset(
      createCommandOption(
        keys[req.headers.key],
        req.headers.username,
        req.headers.quorum
      ),
      {
        srcAccountId: req.body.from,
        destAccountId: `${req.body.to}`,
        assetId: "usd#zoom",
        description: req.body.description,
        amount: req.body.amount.toString()
      }
    );

    console.log("Sending Transfer Command Complete");

    const response = await queries.getAccountAssets(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {
        accountId: req.body.from
      }
    );

    const payloadToResponse = createResponseMessage(
      response[0],
      "New Wallet Info"
    );

    res.status(200).send(payloadToResponse);
  } catch (err) {
    console.log("===> Transafer Error: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

// async function grantAddSignatoryPermissionTo(req, res, next) {
//   try {
//     await commands.grantPermission(
//       createCommandOption(keys[req.headers.key], req.headers.username),
//       {
//         accountId: req.body.to,
//         permission: RolePermission["CAN_ADD_MY_SIGNATORY"]
//       }
//     );
//     await next();
//     const payloadToResponse = createResponseMessage(null);
//     res.status(200).send(payloadToResponse);
//   } catch (err) {
//     console.log("GrandPermissionToError:", err);
//     res.status(400).send(createResponseMessage(err, "Error"));
//   }
// }

// async function addSignatory(req, res) {
//   try {
//     await commands.addSignatory(
//       createCommandOption(keys[req.headers.key], req.headers.username),
//       {
//         accountId: req.body.to,
//         publicKey: req.headers.signatory
//       }
//     );

//     res.status(200).send(createResponseMessage(null));
//   } catch (err) {
//     console.log("AddSignatoryError: ", err);
//     res.status(400).send(createResponseMessage(err, "Error"));
//   }
// }

async function getSignatories(req, res) {
  try {
    const response = await queries.getSignatories(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {
        accountId: req.headers.username
      }
    );
    res.status(200).send(createResponseMessage(response, "Success"));
  } catch (err) {
    console.log("GetSignatoriesError: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

// async function setQuorum(req, res) {
//   try {
//     await commands.setAccountQuorum(
//       createCommandOption(keys[req.headers.key], req.headers.username),
//       {
//         accountId: req.body.to,
//         quorum: req.headers.quorum
//       }
//     );

//     res.status(200).send(createResponseMessage(null));
//   } catch (err) {
//     console.log("SetQuorumError: ", err);
//     res.status(400).send(createResponseMessage(err, "Error"));
//   }
// }

async function getPendingTransactions(req, res) {
  try {
    console.log("Getting transaction for : ", req.headers.username);
    const response = await queries.getPendingTransactions(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {}
    );

    res.status(200).send(createResponseMessage(response, "Success"));
  } catch (err) {
    console.log("GetPendingTransactionsError: ", err);
    res.status(400).send(createResponseMessage(err, "Error"));
  }
}

async function signTransaction(req, res) {
  try {
    console.log("===> Retrieve All Pending Transactions");
    const response = await queries.getRawPendingTransactions(
      createQueryOption(keys[req.headers.key], req.headers.username),
      {}
    );

    if (response.length < 1)
      res
        .status(201)
        .send(createResponseMessage(null, "No Pending Transactions"));

    console.log("All Pendings: ", response.getTransactionsList());

    const txPending = response.getTransactionsList()[0];

    // console.log("PPPPPPPENNNNING: ", txPending.toObject());

    console.log("===> Got first Pending Transaction: ", txPending.toObject());

    let copyTx = sign(txPending, keys[req.headers.key]);

    console.log("===> Sending Transaction");
    txUtil.sendTransactions([copyTx], commandService, 5000);

    res.status(200).send(createResponseMessage(response));
  } catch (err) {
    console.log("SignTransactionError: ", err);
    res.status(400).send(createResponseMessage(err, "Sign Failed"));
  }
}

module.exports = {
  addSignatory,
  getAccountBalance,
  getPendingTransactions,
  grantAddSignatoryPermissionTo,
  getSignatories,
  setQuorum,
  signTransaction,
  customTransferTo,
  transferTo
};
