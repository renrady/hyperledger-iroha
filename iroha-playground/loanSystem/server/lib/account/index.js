const irohaUtil = require("../config");

const {
  queries,
  commands,
  createCommandOption,
  createQueryOption,
  commandService,
  keys
} = irohaUtil;

const RolePermission = require("iroha-helpers/lib/proto/primitive_pb");

const { generateKeyPair } = require("iroha-helpers/lib/cryptoHelper.js");

const createResponseMessage = (payload, message = "Success") => {
  return {
    message: message,
    data: payload
  };
};

async function createAccountInIroha({ username }) {
  try {
    // Generate Keys for new account
    const { publicKey, privateKey } = generateKeyPair();

    // Create Account on IrohaServer
    const response = await commands.createAccount(
      createCommandOption(keys["admin@zoom"], "admin@zoom"),
      {
        accountName: username,
        domainId: "zoom"
      }
    );

    const successPayload = {
      username,
      publicKey,
      privateKey
    };

    // Return information on newly created account
    return successPayload;
  } catch (err) {
    console.log("============= createAccountInIrohaError =============");
    throw err;
  }
}

async function grantAddSignatoryPermissionToAdmin({ privateKey, username }) {
  try {
    await commands.grantPermission(createCommandOption(privateKey, username), {
      accountId: "admin@zoom",
      permission: RolePermission["CAN_ADD_MY_SIGNATORY"]
    });
  } catch (err) {
    console.log(
      "============= grantAddSignatoryPermissionToAdminError ============="
    );
    throw err;
  }
}

async function addSignatory({ username }) {
  try {
    await commands.addSignatory(
      createCommandOption(keys["admin@zoom"], "admin@zoom"),
      {
        accountId: username,
        publicKey: keys["admin@nbc"]
      }
    );
  } catch (err) {
    console.log("============= addSignatoryError =============");
    throw err;
  }
}

async function setQuorum({ username }) {
  try {
    await commands.setAccountQuorum(
      createCommandOption(keys["admin@zoom"], "admin@zoom"),
      {
        accountId: username,
        quorum: 2
      }
    );
  } catch (err) {
    console.log("============= setQuorumError =============");
    throw err;
  }
}

async function CreateAccount(req, res) {
  try {
    console.log("******************* Create Account *******************");

    // Create a new account in IrohaSystem
    const newAccountInfo = await createAccountInIroha(req.body);

    // Grant Add Signatory to ZOOM Admin
    await grantAddSignatoryPermissionToAdmin(newAccountInfo);

    // Add Signatory to NBC
    await addSignatory(newAccountInfo);

    // Set Quorum for new account
    await setQuorum(newAccountInfo);

    res
      .status(200)
      .send(
        createResponseMessage(
          newAccountInfo,
          "Account was successfully created"
        )
      );
  } catch (err) {
    console.log("**************** Create Account Error ****************");
    console.log("Error: ", err);
    console.log("******************************************************");
    res.status(502).send(createResponseMessage(err, "Create account error"));
  }
}

module.exports = { CreateAccount };
