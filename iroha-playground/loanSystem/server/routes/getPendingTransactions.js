var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib/index");

/* GET users signatories. */
router.get("/", irohaFunction.getPendingTransactions);

module.exports = router;
