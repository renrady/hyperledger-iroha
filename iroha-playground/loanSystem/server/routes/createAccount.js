var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib/account/index");

/* POST users transfer. */
router.post("/", irohaFunction.CreateAccount);

module.exports = router;
