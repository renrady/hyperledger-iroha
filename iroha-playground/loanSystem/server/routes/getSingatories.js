var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib/index");

/* GET users signatories. */
router.get("/", irohaFunction.getSignatories);

module.exports = router;
