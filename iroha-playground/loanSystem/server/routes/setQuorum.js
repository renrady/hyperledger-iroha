var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib");

/* POST users transfer. */
router.post("/", irohaFunction.setQuorum);

module.exports = router;
