var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib/index");

/* GET users listing. */
router.get("/", irohaFunction.getAccountBalance);

module.exports = router;
