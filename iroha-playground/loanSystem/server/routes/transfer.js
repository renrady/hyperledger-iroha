var express = require("express");
var router = express.Router();

var irohaFunction = require("../lib/index");

/* POST users transfer. */
router.post("/", irohaFunction.customTransferTo);

module.exports = router;
