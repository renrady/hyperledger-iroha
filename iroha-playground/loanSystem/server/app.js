var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var walletRouter = require("./routes/wallet");
var transferRouter = require("./routes/transfer");
var grantSignatoryRouter = require("./routes/grantSignatoryPermission");
var addSignatory = require("./routes/addSignatory");
var getSignatories = require("./routes/getSingatories");
var setQuorum = require("./routes/setQuorum");
var getPendingTransactions = require("./routes/getPendingTransactions");
var signTransaction = require("./routes/signTransaction");
var createAccount = require("./routes/createAccount");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/wallet", walletRouter);
app.use("/transfer", transferRouter);
app.use("/grant-signatory", grantSignatoryRouter);
app.use("/add-signatory", addSignatory);
app.use("/signatories", getSignatories);
app.use("/set-quorum", setQuorum);
app.use("/pending-transactions", getPendingTransactions);
app.use("/sign", signTransaction);
app.use("/createAccount", createAccount);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
