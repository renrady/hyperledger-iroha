# Setup Block

```
    docker volume create zoom_block

    docker volume create amk_block

    docker volume create kredit_block

    docker volume create prasac_block
```

# Create Container

ZOOM Node

```
    docker run --name zoom-one \
    -p 50051:50051 \
    -p 10001:10001 \
    -v ~/iroha-playground/loanSystem/zoom/node1:/opt/iroha_data \
    -v zoom_1:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='zoom_node_1' \
    --network=iroha-network \
    -d hyperledger/iroha:latest

    docker run --name nbc-one \
    -p 50054:50054 \
    -p 10004:10004 \
    -v ~/iroha-playground/loanSystem/nbc/node1:/opt/iroha_data \
    -v nbc_1:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='nbc_node_1' \
    --network=iroha-network \
    -d hyperledger/iroha:latest

```

AMK Node

```
    docker run --name amk \
    -p 50056:50056 \
    -p 10006:10006 \
    -v ~/iroha-playground/tree/amk:/opt/iroha_data \
    -v amk_block:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='amk_node' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0
```

Kredit Node

```
    docker run --name kredit \
    -p 50057:50057 \
    -p 10007:10007 \
    -v ~/iroha-playground/tree/kredit:/opt/iroha_data \
    -v kredit_block:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='kredit_node' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0
```

Prasac Node

```
    docker run --name prasac \
    -p 50058:50058 \
    -p 10008:10008 \
    -v ~/iroha-playground/tree/prasac:/opt/iroha_data \
    -v prasac_block:/tmp/block_store \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    -e KEY='prasac_node' \
    --network=iroha-network \
    -d hyperledger/iroha:1.0.0
```

CMD Node for Sending Cmd and Query

```
    docker run -it --name cmd \
    -p 50057:50057 \
    -p 10007:10007 \
    -v ~/iroha-playground/loanSystem/cmd:/opt/iroha_data \
    -e POSTGRES_HOST='some-postgres' \
    -e POSTGRES_PORT='5432' \
    -e POSTGRES_PASSWORD='mysecretpassword' \
    -e POSTGRES_USER='postgres' \
    --network=iroha-network \
    --entrypoint=/bin/bash \
    -d hyperledger/iroha:1.0.0
```
